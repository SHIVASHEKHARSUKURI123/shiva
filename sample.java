package bank;

import bank1.InsufficientBalanceException;

public class Account  {
	
	private String name;
	private int balance;
	private String id="VPB000";
	private static int count=0;
	public Account(){
		count++;
		name="Shiva";
		balance=1000;
		id=id+count;
	}
	public Account(String name){
		count++;
		this.name=name;
		this.id=id+count;
	}
	public Account(String name,int balance){
		count++;
		this.balance=balance;
		this.name=name;
		this.id=id+count;


	}
	public Account(String name,int balance,String id){
		count++;
		this.name=name;
		this.balance=balance;
		this.id=id+count;
	}
	

	public String getName(){
		return name;
	}
	public double getBalance(){
		return balance;
	}
	public String getId(){
		return id;
	}
	public void setName(String name){
		this.name=name;
	}
	public void setBalance(int balance){
		this.balance=balance;
	}
	public static int getCount(){
		return count;
	}
	public int credit(int amount){

		balance=balance+amount;

		return balance;
	}
	public int debit(int amount) throws InsufficientBalanceException {
		if(amount<=balance){
			balance=balance-amount;
		}
		else{
			
			throw (new InsufficientBalanceException("unable to deposite"));
			
			}
		return balance;
	}
package exception;

public class InvalidAgeException extends Exception{
	public InvalidAgeException(String message){
		super(message);
	}

}
private String name;
	private int balance;
	private String id="VPB000";
	private static int count=0;
	public Account(){
		count++;
		name="Shiva";
		balance=1000;
		id=id+count;
